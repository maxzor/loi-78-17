<!-- TITLE: Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés</span> <a href="/loi-78-17/chapitre-ier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>

* [Entête](/loi-78-17/entete)
* [Visas](/loi-78-17/visas)

* [Chapitre Ier — Principes et définitions](/loi-78-17/chapitre-ier)

  * [Article 1](/loi-78-17/chapitre-ier/article-1)
  * [Article 2](/loi-78-17/chapitre-ier/article-2)
  * [Article 3](/loi-78-17/chapitre-ier/article-3)
  * [Article 4](/loi-78-17/chapitre-ier/article-4)
  * [Article 5](/loi-78-17/chapitre-ier/article-5)

* [Chapitre II — Conditions de licéité des traitements de données à caractère personnel](/loi-78-17/chapitre-ii)

  * [Section 1 — Dispositions générales](/loi-78-17/chapitre-ii/section-1)

    * [Article 6](/loi-78-17/chapitre-ii/section-1/article-6)
    * [Article 7](/loi-78-17/chapitre-ii/section-1/article-7)

  * [Section 2 — Dispositions propres à certaines catégories de données](/loi-78-17/chapitre-ii/section-2)

    * [Article 8](/loi-78-17/chapitre-ii/section-2/article-8)
    * [Article 9](/loi-78-17/chapitre-ii/section-2/article-9)
    * [Article 10](/loi-78-17/chapitre-ii/section-2/article-10)

* [Chapitre II — La Commission nationale de l'informatique et des libertés (abrogé)](/loi-78-17/chapitre-ii-abroge)

* [Chapitre III — La Commission nationale de l'informatique et des libertés.](/loi-78-17/chapitre-iii)

  * [Article 11](/loi-78-17/chapitre-iii/article-11)
  * [Article 12 (abrogé)](/loi-78-17/chapitre-iii/article-12-abroge)
  * [Article 13](/loi-78-17/chapitre-iii/article-13)
  * [Article 14 (abrogé)](/loi-78-17/chapitre-iii/article-14-abroge)
  * [Article 15](/loi-78-17/chapitre-iii/article-15)
  * [Article 15 bis](/loi-78-17/chapitre-iii/article-15-bis)
  * [Article 16](/loi-78-17/chapitre-iii/article-16)
  * [Article 17](/loi-78-17/chapitre-iii/article-17)
  * [Article 18](/loi-78-17/chapitre-iii/article-18)
  * [Article 19](/loi-78-17/chapitre-iii/article-19)
  * [Article 20](/loi-78-17/chapitre-iii/article-20)
  * [Article 21](/loi-78-17/chapitre-iii/article-21)

* [LA COMMISSION NATIONALE DE L'INFORMATIQUE ET DES LIBERTES (abrogé)](/loi-78-17/la-commission-nationale-de-l-informatique-et-des-libertes-abroge)

* [Chapitre III — Formalités préalables à la mise en oeuvre des traitements automatisés. (abrogé)](/loi-78-17/chapitre-iii-abroge)

* [Chapitre IV — Formalités préalables à la mise en oeuvre des traitements.](/loi-78-17/chapitre-iv)

  * [Article 22](/loi-78-17/chapitre-iv/article-22)

  * [Section 1 — Déclaration.](/loi-78-17/chapitre-iv/section-1)

    * [Article 23](/loi-78-17/chapitre-iv/section-1/article-23)
    * [Article 24](/loi-78-17/chapitre-iv/section-1/article-24)

  * [Section 2 — Autorisation.](/loi-78-17/chapitre-iv/section-2)

    * [Article 25](/loi-78-17/chapitre-iv/section-2/article-25)
    * [Article 26](/loi-78-17/chapitre-iv/section-2/article-26)
    * [Article 27](/loi-78-17/chapitre-iv/section-2/article-27)
    * [Article 28](/loi-78-17/chapitre-iv/section-2/article-28)
    * [Article 29](/loi-78-17/chapitre-iv/section-2/article-29)

  * [Section 3 — Dispositions communes.](/loi-78-17/chapitre-iv/section-3)

    * [Article 30](/loi-78-17/chapitre-iv/section-3/article-30)
    * [Article 31](/loi-78-17/chapitre-iv/section-3/article-31)

* [Chapitre IV — Collecte, enregistrement et conservation des informations nominatives. (abrogé)](/loi-78-17/chapitre-iv-abroge)

  * [Article 29-1 (abrogé)](/loi-78-17/chapitre-iv-abroge/article-29-1-abroge)
  * [Article 33-1 (abrogé)](/loi-78-17/chapitre-iv-abroge/article-33-1-abroge)

* [Chapitre V — Obligations incombant aux responsables de traitements et droits des personnes](/loi-78-17/chapitre-v)

  * [Section 1 — Obligations incombant aux responsables de traitements.](/loi-78-17/chapitre-v/section-1)

    * [Article 32](/loi-78-17/chapitre-v/section-1/article-32)
    * [Article 33](/loi-78-17/chapitre-v/section-1/article-33)
    * [Article 34](/loi-78-17/chapitre-v/section-1/article-34)
    * [Article 34 bis](/loi-78-17/chapitre-v/section-1/article-34-bis)
    * [Article 35](/loi-78-17/chapitre-v/section-1/article-35)
    * [Article 36](/loi-78-17/chapitre-v/section-1/article-36)
    * [Article 37](/loi-78-17/chapitre-v/section-1/article-37)

  * [Section 2 — Droits des personnes à l'égard des traitements de données à caractère personnel.](/loi-78-17/chapitre-v/section-2)

    * [Article 38](/loi-78-17/chapitre-v/section-2/article-38)
    * [Article 39](/loi-78-17/chapitre-v/section-2/article-39)
    * [Article 40](/loi-78-17/chapitre-v/section-2/article-40)
    * [Article 40-1](/loi-78-17/chapitre-v/section-2/article-40-1)
    * [Article 41](/loi-78-17/chapitre-v/section-2/article-41)
    * [Article 42](/loi-78-17/chapitre-v/section-2/article-42)
    * [Article 43](/loi-78-17/chapitre-v/section-2/article-43)
    * [Article 43 bis](/loi-78-17/chapitre-v/section-2/article-43-bis)
    * [Article 43 ter](/loi-78-17/chapitre-v/section-2/article-43-ter)

* [Chapitre V — Exercice du droit d'accès. (abrogé)](/loi-78-17/chapitre-v-abroge)

* [Chapitre V bis — Traitements automatisés de données nominatives ayant pour fin la recherche dans le domaine de la santé. (abrogé)](/loi-78-17/chapitre-v-bis-abroge)

  * [Article 40-9 (abrogé)](/loi-78-17/chapitre-v-bis-abroge/article-40-9-abroge)
  * [Article 40-10 (abrogé)](/loi-78-17/chapitre-v-bis-abroge/article-40-10-abroge)

* [Chapitre V ter — Traitement des données personnelles de santé à des fins d'évaluation ou d'analyse des activités de soins et de prévention. (abrogé)](/loi-78-17/chapitre-v-ter-abroge)

* [Chapitre VI — Dispositions pénales. (abrogé)](/loi-78-17/chapitre-vi-abroge)

* [Chapitre VI — Le contrôle de la mise en oeuvre des traitements.](/loi-78-17/chapitre-vi)

  * [Article 44](/loi-78-17/chapitre-vi/article-44)

* [Chapitre VII — Dispositions diverses. (abrogé)](/loi-78-17/chapitre-vii-abroge)

* [Chapitre VII — Sanctions prononcées par la formation restreinte de la Commission nationale de l'informatique et des libertés.](/loi-78-17/chapitre-vii)

  * [Article 45](/loi-78-17/chapitre-vii/article-45)
  * [Article 46](/loi-78-17/chapitre-vii/article-46)
  * [Article 47](/loi-78-17/chapitre-vii/article-47)
  * [Article 48](/loi-78-17/chapitre-vii/article-48)
  * [Article 49](/loi-78-17/chapitre-vii/article-49)
  * [Article 49 bis](/loi-78-17/chapitre-vii/article-49-bis)

* [Chapitre VIII — Dispositions pénales.](/loi-78-17/chapitre-viii)

  * [Article 50](/loi-78-17/chapitre-viii/article-50)
  * [Article 51](/loi-78-17/chapitre-viii/article-51)
  * [Article 52](/loi-78-17/chapitre-viii/article-52)

* [Chapitre IX — Traitements de données à caractère personnel  à des fins de recherche, d'étude ou d'évaluation dans le domaine de la santé.](/loi-78-17/chapitre-ix)

  * [Article 53](/loi-78-17/chapitre-ix/article-53)
  * [Article 54](/loi-78-17/chapitre-ix/article-54)
  * [Article 55](/loi-78-17/chapitre-ix/article-55)
  * [Article 56](/loi-78-17/chapitre-ix/article-56)
  * [Article 57](/loi-78-17/chapitre-ix/article-57)
  * [Article 58](/loi-78-17/chapitre-ix/article-58)
  * [Article 59](/loi-78-17/chapitre-ix/article-59)
  * [Article 60](/loi-78-17/chapitre-ix/article-60)
  * [Article 61](/loi-78-17/chapitre-ix/article-61)

* [Chapitre X — Traitements de données de santé à caractère personnel à des fins d'évaluation ou d'analyse des pratiques ou des activités de soins et de prévention. (abrogé)](/loi-78-17/chapitre-x-abroge)

  * [Article 62 (abrogé)](/loi-78-17/chapitre-x-abroge/article-62-abroge)
  * [Article 63 (abrogé)](/loi-78-17/chapitre-x-abroge/article-63-abroge)
  * [Article 64 (abrogé)](/loi-78-17/chapitre-x-abroge/article-64-abroge)
  * [Article 65 (abrogé)](/loi-78-17/chapitre-x-abroge/article-65-abroge)
  * [Article 66 (abrogé)](/loi-78-17/chapitre-x-abroge/article-66-abroge)

* [Chapitre XI — Traitements de données à caractère personnel aux fins de journalisme et d'expression littéraire et artistique.](/loi-78-17/chapitre-xi)

  * [Article 67](/loi-78-17/chapitre-xi/article-67)

* [Chapitre XII — Transferts de données à caractère personnel vers des Etats n'appartenant pas à la Communauté européenne.](/loi-78-17/chapitre-xii)

  * [Article 68](/loi-78-17/chapitre-xii/article-68)
  * [Article 69](/loi-78-17/chapitre-xii/article-69)
  * [Article 70](/loi-78-17/chapitre-xii/article-70)

* [Chapitre XIII — Dispositions diverses.](/loi-78-17/chapitre-xiii)

  * [Article 71](/loi-78-17/chapitre-xiii/article-71)
  * [Article 72](/loi-78-17/chapitre-xiii/article-72)

* [Signataires](/loi-78-17/signataires)
* [Nota](/loi-78-17/nota)

<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

* Avis sur un projet de loi d’adaptation au droit de l’Union européenne de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
  * [6.](/avis-conseil-etat-393836/choix-legistiques-et-propositions-pour-une-meilleure-lisibilite-du-droit/6)
  * [7.](/avis-conseil-etat-393836/choix-legistiques-et-propositions-pour-une-meilleure-lisibilite-du-droit/7)
  * [9.](/avis-conseil-etat-393836/choix-legistiques-et-propositions-pour-une-meilleure-lisibilite-du-droit/9)
  * [1.](/avis-conseil-etat-393836/presentation-generale/1)
* Délibération n°2017-299 du 30 novembre 2017 — Commission Nationale de l'Informatique et des Libertés
  * [Considérants](/deliberation-cnil-2017-299/considerants)
  * [Introduction](/deliberation-cnil-2017-299/introduction)
* Étude d'impact - N° 490 - Projet de loi relatif à la protection des données personnelles
  * [INTRODUCTION GENERALE](/etude-impact-490/introduction-generale)
  * [ARTICLE 1ER — MISSIONS DE LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTES](/etude-impact-490/titre-ier/chapitre-ier/article-1er)
  * [ARTICLE 4 — POUVOIRS DE CONTRÔLE DE LA CNIL](/etude-impact-490/titre-ier/chapitre-ier/article-4)
  * [ARTICLE 5 — PROCÉDURE DE COOPÉRATION DE LA CNIL AVEC LES AUTRES AUTORITÉS DE CONTRÔLE](/etude-impact-490/titre-ier/chapitre-ier/article-5)
  * [ARTICLE 9 — ALLEGEMENT DES FORMALITES PREALABLES](/etude-impact-490/titre-ii/chapitre-ii/article-9)
  * [ARTICLE 10 — SOUS-TRAITANT](/etude-impact-490/titre-ii/chapitre-iii/article-10)
  * [ARTICLE 13 — TRAITEMENTS DE DONNEES DE SANTE](/etude-impact-490/titre-ii/chapitre-iv/article-13)
  * [ARTICLE 14 — DECISION ADMINISTRATIVE AUTOMATISEE](/etude-impact-490/titre-ii/chapitre-iv/article-14)
  * [ARTICLE 15 — LIMITATION DES DROITS](/etude-impact-490/titre-ii/chapitre-iv/article-15)
  * [ARTICLE 18 ET ARTICLE 19 SECTION 3 — DROITS DE LA PERSONNE CONCERNÉE](/etude-impact-490/titre-iii/article-18-et-article-19-section-3)
  * [ARTICLE 19 SECTION 2 — OBLIGATIONS INCOMBANT AUX AUTORITES COMPETENTES ET AUX RESPONSABLES DE TRAITEMENT](/etude-impact-490/titre-iii/article-19-section-2)
  * [ARTICLE 19 SECTION 4 — TRANSFERTS INTERNATIONAUX](/etude-impact-490/titre-iii/article-19-section-4)
  * [ARTICLE 20](/etude-impact-490/titre-iv/article-20)
  * [ARTICLE 22 — MISE A DISPOSITION DE LA LISTE DES TRAITEMENTS AYANT FAIT L’OBJET DE FORMALITES PREALABLES](/etude-impact-490/titre-v/article-22)
* N° 490 - Projet de loi relatif à la protection des données personnelles
  * [Exposé des motifs](/pjl-490/expose-des-motifs)
  * [Article 20](/pjl-490/titre-iv/article-20)
  * [Article 21](/pjl-490/titre-v/article-21)

<!-- FIN REFERENCES -->

<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%"> <span> </span> <span style="text-align: center; text-transform: uppercase;">Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés</span> <a href="/loi-78-17/chapitre-ier" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a> </div>
